#include <stdio.h> 
#include <stdlib.h> 

// looping : left shift 
int main(void) 
{
    int i; 
    int n = 15; 
    int k = 3; 

    puts("Block 1"); 
    for(i = 1; i < 256; i = i << 2)
        printf("i = %d\n", i); 
    /*
        Block 1
        i = 1
        i = 4
        i = 16
        i = 64
    */

    puts("Block 2"); 
    for(i = 1; i <= 256; i <<= 2)
        printf("i = %d\n", i); 
    /*
        Block 2
        i = 1
        i = 4
        i = 16
        i = 64
        i = 256
    */

    puts("Block 3"); 
    for(i = 1; i < n; i <<= k)
        printf("i = %d\n", i); 
    /*
        Block 3
        i = 1
        i = 8
    */

    puts("Block 4"); 
    for(i = 1; i <= n; i <<= n)
        printf("i = %d\n", i); 
    /*
        Block 4
        i = 1
    */
    return (0);
}

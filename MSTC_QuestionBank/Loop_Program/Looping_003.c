#include <stdio.h> 

// looping multiplication 
int main(void)
{
    int i; 
    int n; 
    int k; 

    puts("block 1"); 
    for(i = 1; i < 100; i = i * 2)
        printf("i = %d\n", i); 
    /*
        block 1
        i = 1
        i = 2
        i = 4
        i = 8
        i = 16
        i = 32
        i = 64
    */

    puts("block 2"); 
    for(i = 1; i <= 256; i *= 2)
        printf("i = %d\n", i); 
    /*
        block 2
        i = 1
        i = 2
        i = 4
        i = 8
        i = 16
        i = 32
        i = 64
        i = 128
        i = 256
    */

    puts("block 3"); 
    printf("Enter n:"); 
    scanf("%d", &n); 
    printf("Enter k:"); 
    scanf("%d", &k); 
    for(i = 1; i < n; i = i * k)
        printf("i = %d\n", i); 
    for(i = 1; i <= n; i *= k)
        printf("i = %d\n", i); 
    /*
        block 3
        n = 10
        k = 2
        i = 1
        i = 2
        i = 4
        i = 8
        i = 1
        i = 2
        i = 4
        i = 8
    */
    return (0); 
}
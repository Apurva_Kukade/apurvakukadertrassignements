#include <stdio.h> 

// looping division 
int main(void)
{
    int i; 
    int n; 
    int k; 

    puts("block 1"); 
    // what will happen if condition is i >= 0?? 
    for(i = 100; i > 0; i = i / 2)
        printf("i = %d\n", i); 
    /*
        block 1
        i = 100
        i = 50
        i = 25
        i = 12
        i = 6
        i = 3
        i = 1
    */
    puts("block 2"); 
    printf("Enter n:"); 
    scanf("%d", &n); 
    printf("Enter k:"); 
    scanf("%d", &k); 
    for(i = n; i > 0; i = i / k)
        printf("i = %d\n", i); 
    /*
        block 2
        n = 20
        k = 4
        i = 20
        i = 5
        i = 1
    */
    return (0); 
}
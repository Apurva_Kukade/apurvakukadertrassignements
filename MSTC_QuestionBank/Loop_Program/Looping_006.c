#include <stdio.h> 

// looping : right shift 
int main(void)
{
    int i; 
    int n = 512; 
    int k = 3; 

    puts("block 1"); 
    for(i = 256; i > 0; i = i >> 2) 
        printf("i = %d\n", i); 
    /*
        block 1
        i = 256
        i = 64
        i = 16
        i = 4
        i = 1
    */

    puts("block 2"); 
    for(i = n; i > 0; i >>= k)
        printf("i = %d\n", i);    
    /*
        block 2
        i = 512
        i = 64
        i = 8
        i = 1
    */
    return (0); 
}
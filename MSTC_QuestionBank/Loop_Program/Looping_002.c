#include <stdio.h> 

// looping subtraction 
int main(void)
{
    int i;  
    int n; 
    int k; 

    puts("block 1"); 
    for(i = 5; i > 0; i--)
        printf("i = %d\n", i); 
    /*
        block 1
        i = 5
        i = 4
        i = 3
        i = 2
        i = 1
    */

    puts("block 2"); 
    for(i = 5; i >= 0; --i)
        printf("i = %d\n", i); 
    /*
        block 2
        i = 5
        i = 4
        i = 3
        i = 2
        i = 1
        i = 0
    */

    puts("block 3"); 
    for(i = 10; i >= 0; i -= 2)
        printf("i = %d\n", i); 

    /*
        block 3
        i = 10
        i = 8
        i = 6
        i = 4
        i = 2
        i = 0
    */

    puts("block 4"); 
    for(i = 10; i > 0; i = i - 2)
        printf("i = %d\n", i); 
    /*
        block 4
        i = 10
        i = 8
        i = 6
        i = 4
        i = 2
    */

    puts("block 5"); 
    printf("Enter n:"); 
    scanf("%d", &n); 
    printf("Enter k:"); 
    scanf("%d", &k); 
    for(i = n; i > 0; i = i - k)
        printf("i = %d\n", i); 
    for(i = n; i >= 0; i -= k)
        printf("i = %d\n", i); 
    /*
        block 5
        n = 10
        k = 3
        i = 10
        i = 7
        i = 4
        i = 1
        i = 10
        i = 7
        i = 4
        i = 1

    */
    return (0); 
}
#include <stdio.h> 

// for - summations 
int main(void) 
{
    int i; 
    int n; 
    int k; 

    puts("block 1"); 
    for(i = 0; i < 5; ++i)
        printf("i = %d\n", i); 
    /* 
        block1
        i = 0
        i = 1
        i = 2
        i = 3
        i = 4
    */

    puts("block 2");
    for(i = 0; i <= 10; i = i + 2)
        printf("i = %d\n", i); 
    /*
        block 2
        i = 0
        i = 2
        i = 4
        i = 6
        i = 8
        i = 10
    */

    puts("block 3");
    for(i = 1; i <= 10; i += 2)
        printf("i = %d\n", i);
    /*
        block 3
        i = 1
        i = 3
        i = 5
        i = 7
        i = 9
    */

    puts("block 4"); 
    printf("Enter n:"); 
    scanf("%d", &n); 
    printf("Enter k:");
    scanf("%d", &k); 
    for(i = 0; i < n; i += k)
        printf("i = %d\n", i); 
    /*
        block 4
        20
        5
        i = 0
        i = 5
        i = 10
        i = 15
    */
    return (0); 
}
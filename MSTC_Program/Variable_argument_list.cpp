#include <windows.h>
#include <stdarg.h>
#include <tchar.h>
#include <stdio.h>

#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")

int CDECL MessageBoxPrint(TCHAR* szCaption, TCHAR* szFormat, ...)
{
	va_list arglist;
	TCHAR buffer[100];

	va_start(arglist, szFormat);		// last named parameter cha address arflist madhe tevhla

	vsprintf(buffer, szFormat, arglist);
	
	va_end(arglist);

	return MessageBox((HWND)NULL, buffer, szCaption, MB_OK);
}

int WINAPI WinMain(
					HINSTANCE hInstance,
					HINSTANCE hPrevInstance,
					LPSTR lpszCmdLine,
					int iCmdShow
				   )
{
	int cxScreen, cyScreen;

	cxScreen = GetSystemMetrics(SM_CXSCREEN);
	cyScreen = GetSystemMetrics(SM_CYSCREEN);

	// MessageBoxPrint(TCHAR* szCaption, TCHAR* szBody, ...);
	MessageBoxPrint(TEXT("Screen size"),TEXT("My screen size is %d"), cxScreen, cyScreen);

	return EXIT_SUCCESS;
}


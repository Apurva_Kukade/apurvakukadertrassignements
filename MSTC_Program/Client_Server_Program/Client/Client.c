#include <stdio.h>
#include "Server.h"

int main(void)
{
	int iResult;
	struct Operation op;

	Get(&op);

	iResult = Add(&op);
	printf("Addition of given number is %d\t\n", iResult);

	iResult = Sub(&op);
	printf("Subtraction of given number is %d\t\n", iResult);

	iResult = Mult(&op);
	printf("Multiplication of given number is %d\t\n", iResult);

	iResult = Div(&op);
	printf("Division of given number is %d\t\n", iResult);

	return 0;
}
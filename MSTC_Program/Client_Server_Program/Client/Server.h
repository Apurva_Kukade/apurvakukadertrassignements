#ifndef __SERVER_H
#define __SERVER_H
struct Operation
{
	int iNo1;
	int iNo2;
};

extern void Get(struct Operation *op);
extern int Add(struct Operation *op);
extern int Sub(struct Operation *op);
extern int Mult(struct Operation *op);
extern int Div(struct Operation *op);

#endif
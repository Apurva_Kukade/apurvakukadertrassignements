//
// Program 1 : Assuming that the bit numbering starts from 1. write a c program to set a perticular bit 
// 			   in a given number.
//

#include <stdio.h>

int main(void)
{
	int iBit = 0x01;
	int iNo;
	int iBitPos;
	
	printf("Enter decimal number : ");
	scanf("%d", &iNo);

	printf("Enter bit position to set : ");
	scanf("%d", &iBitPos);

	iBit = iBit << (iBitPos-1);
	iNo = iNo | iBit;
	printf("After Set given bit the given number is %d\n",iNo);
	
	return 0;
}
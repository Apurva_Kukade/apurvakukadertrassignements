//
// Program 4 :  Given two numbers , say key and val. wherever the bits of number key are 1 set
//				the corresponding bits of number val. 
//				Leave all other bits of number val unchanged.
//
//	output :  	val -> 3
//			  	Key -> 8
//			  	Answer -> 11

#include <stdio.h>

int main(void)
{
	int iVal;
	int iKey;
	
	printf("Enter val number : ");
	scanf("%d", &iVal);

	printf("Enter Key number : ");
	scanf("%d", &iKey);

	iVal = iVal | iKey;
	printf("After Set given bit the val is %d\n",iVal);
	
	return 0;
}
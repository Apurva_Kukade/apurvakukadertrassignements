//
// Program 3 :  Given two numbers , say key and val. wherever the bits of number key are 1 reset
//				(i.e make 0) the corresponding bits of number val. 
//				Leave all other bits of number val unchanged.
//
// output :  val -> 7
//			 Key -> 5
//			 Answer -> 2

#include <stdio.h>

int main(void)
{
	int iVal;
	int iKey;
	
	printf("Enter val number : ");
	scanf("%d", &iVal);

	printf("Enter Key number : ");
	scanf("%d", &iKey);

	iKey = ~iKey;
	iVal = iVal & iKey;
	printf("After Reseting given bit the val is %d\n",iVal);
	
	return 0;
}
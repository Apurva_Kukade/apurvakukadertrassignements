#include <Windows.h>

#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	static TCHAR szClassName[] = TEXT("The first window");
	static TCHAR szWindowCaption[] = TEXT("Hello windows!!!!!");

	HBRUSH hBrush = NULL;
	HCURSOR hCursor = NULL;
	HICON hIcon = NULL;
	HICON hIconSm = NULL;
	HWND hWnd = NULL;

	WNDCLASSEX wndEx;
	MSG msg;

	ZeroMemory(&wndEx, sizeof(WNDCLASSEX));
	ZeroMemory(&msg, sizeof(MSG));

	hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH);
	if(hBrush == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Could not optain brush"), TEXT("GetStockObject"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	hCursor = LoadCursor((HINSTANCE)NULL, IDC_ARROW);
	if(hCursor == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Could not optain cursor"), TEXT("LoadCursor"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	if(hIcon == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Could not optain icon"), TEXT("LoadIcon"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	hIconSm = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	if(hIconSm == NULL)
	{
		MessageBox((HWND)NULL,TEXT("could not optain small icon"), TEXT("LoadIcon"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	wndEx.cbSize = sizeof(WNDCLASSEX);
	wndEx.cbClsExtra = 0;					// extra byte for window class structure
	wndEx.cbWndExtra = 0;					// extra byte to allocate that window instance
	wndEx.hbrBackground = hBrush;	
	wndEx.hCursor = hCursor;
	wndEx.hIcon = hIcon;
	wndEx.hIconSm = hIconSm;
	wndEx.hInstance = hInstance;
	wndEx.lpfnWndProc = WndProc;
	wndEx.lpszClassName = szClassName;
	wndEx.lpszMenuName = NULL; 
	wndEx.style = CS_HREDRAW | CS_VREDRAW;

	if(!RegisterClassEx(&wndEx))
	{
		MessageBox((HWND)NULL, TEXT("Could not register window class"), TEXT("RegisterClassEx"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	hWnd = CreateWindowEx(
							WS_EX_APPWINDOW,
							szClassName,				// window class 
							szWindowCaption,			// window caption
							WS_OVERLAPPEDWINDOW,		// window style
							CW_USEDEFAULT,				// x position
							CW_USEDEFAULT,				// Y position
							CW_USEDEFAULT,				// width
							CW_USEDEFAULT,				// height
							(HWND)NULL,					// handle of owner window
							(HMENU)NULL,				// handle of menu
							hInstance,					// handle of instance with window
							NULL
						 );

	if(hWnd == NULL)
	{
		MessageBox((HWND)NULL, TEXT("Could not create application window"),TEXT("CreateWindow"), MB_ICONERROR);
		return (EXIT_FAILURE);
	}

	ShowWindow(hWnd, nShowCmd);
	UpdateWindow(hWnd);

	while(GetMessage(&msg, (HWND)NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (msg.wParam);
}

LRESULT CALLBACK WndProc(
							HWND hWnd,
							UINT uMsg,
							WPARAM wParam,
							LPARAM lParam
						)
{
	switch(uMsg)
	{
		case WM_DESTROY:
			 PostQuitMessage(EXIT_SUCCESS);
			 break;
	}

	return (DefWindowProc(hWnd, uMsg, wParam, lParam));
}